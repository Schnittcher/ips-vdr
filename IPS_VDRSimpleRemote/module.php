<?
class IPS_VDRSimpleRemote extends IPSModule {

  public function Create(){
    //Never delete this line!
    parent::Create();
    $this->ConnectParent("{A9EAA472-5694-49FA-8D90-1D5AC1A89915}");
    $this->RegisterTimer("UpdateChannelInfo", 100000, 'VDRSimpleRemote_ChannelInfo($_IPS[\'TARGET\']);');
    $this->RegisterPropertyString("Stationen", "");
  }

  public function ApplyChanges() {
    //Never delete this line!
    parent::ApplyChanges();

    $arrString = $this->ReadPropertyString("Stationen");
    $Stationen = json_decode($arrString);
    IPS_LogMessage("Stationen", $arrString);
    $Profile = [];
      foreach($Stationen as $Station) {
        $tmp = Array($Station->kanalnummer, $Station->stationsname, "", -1);
        array_push($Profile,$tmp);
      }
      $this->RegisterProfileIntegerEx("VDR.Stationen","Information","","",$Profile);
      $variablenIDStation = $this->RegisterVariableInteger("VDRStation", "Station", "VDR.Stationen");
      $this->EnableAction("VDRStation");
      $variablenIDKanal = $this->RegisterVariableString("VDRKanal", "Kanal");
      $variablenIDTitel = $this->RegisterVariableString("VDRTitel", "Titel");
}

  public function ReceiveData($JSONString) {
    $data = json_decode($JSONString);
    if ($data->Action == "getChannelInfo") {
      SetValue($this->GetIDForIdent("VDRKanal"), $data->Buffer->Channel);
      SetValue($this->GetIDForIdent("VDRTitel"), $data->Buffer->Titel);
      //SetValue($this->GetIDForIdent("VDRStation"), $StationNumber);
      //SetValue($this->GetIDForIdent("VDRStation"), $StationNumber);

    }
  }

  public function Station($StationNumber) {
    SetValue($this->GetIDForIdent("VDRStation"), $StationNumber);
    @$this->SendDataToParent(json_encode(Array("DataID" => "{66900AB7-4164-4AB3-9F86-703A38CD5DA0}", "Action" => "Station", "Buffer" => $StationNumber)));
  }

  public function ChannelInfo() {
    @$this->SendDataToParent(json_encode(Array("DataID" => "{66900AB7-4164-4AB3-9F86-703A38CD5DA0}", "Action" => "ChannelInfo")));
  }

  protected function RegisterProfileInteger($Name, $Icon, $Prefix, $Suffix, $MinValue, $MaxValue, $StepSize) {
    if(!IPS_VariableProfileExists($Name)) {
      IPS_CreateVariableProfile($Name, 1);
    }
    else {
      $profile = IPS_GetVariableProfile($Name);
      if($profile['ProfileType'] != 1)
      throw new Exception("Variable profile type does not match for profile ".$Name);
    }
    IPS_SetVariableProfileIcon($Name, $Icon);
    IPS_SetVariableProfileText($Name, $Prefix, $Suffix);
    IPS_SetVariableProfileValues($Name, $MinValue, $MaxValue, $StepSize);
    }



  protected function RegisterProfileIntegerEx($Name, $Icon, $Prefix, $Suffix, $Associations) {
    if ( sizeof($Associations) === 0 ){
      $MinValue = 0;
      $MaxValue = 0;
    }
    else {
      $MinValue = $Associations[0][0];
      $MaxValue = $Associations[sizeof($Associations)-1][0];
    }
    $this->RegisterProfileInteger($Name, $Icon, $Prefix, $Suffix, $MinValue, $MaxValue, 0);
    foreach($Associations as $Association) {
      IPS_SetVariableProfileAssociation($Name, $Association[0], $Association[1], $Association[2], $Association[3]);
    }
  }

  public function RequestAction($Ident, $Value) {
    switch ($Ident) {
      case "VDRStation":
        $result = $this->Station($Value);
        break;
      default:
        throw new Exception("Invalid ident");
    }
  }
}
?>
