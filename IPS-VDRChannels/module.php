<?
class IPS_VDRChannels extends IPSModule {

  public function Create(){
    //Never delete this line!
    parent::Create();
    $this->ConnectParent("{A9EAA472-5694-49FA-8D90-1D5AC1A89915}");
  }

  public function ApplyChanges() {
    //Never delete this line!
    parent::ApplyChanges();
    $this->RegisterVariableString("Kanaele", "Kanaele", "~HTMLBox");

    $sid = $this->RegisterScript("WebHookVDRREPG", "WebHookVDREPG", '<? //Do not delete or modify.
    if (isset($_GET["ChannelID"]))
    VDRChannels_EPGHook(' . $this->InstanceID . ',$_GET["ChannelID"]);
    ', -8);
    IPS_SetHidden($sid, true);
    $this->RegisterHook('/hook/VDREPG' . $this->InstanceID, $sid);


    /**    $MyFuncs=array('getChannels','getChannelGroups','getEPG');
    $Lines = array();
    foreach ($MyFuncs as $Func)
    {
    $Lines[] = '.*"Action":"' . $Func. '".*';
  }
  $Line = implode('|', $Lines);
  $this->SetReceiveDataFilter("(" . $Line . ")");
  $this->SendDebug("SetFilter", "(" . $Line . ")", 0); **/
}

private function getWebhookLink($ChannelID) {
  return 'onclick="window.xhrGet=function xhrGet(o) {var HTTP = new XMLHttpRequest();HTTP.open(\'GET\',o.url,true);HTTP.send();};window.xhrGet({ url: \'hook/VDREPG' . $this->InstanceID . '?ChannelID=' . $ChannelID .'\'})"';
}

public function ReceiveData($JSONString) {
  $data = json_decode($JSONString);
  if ($data->Action == "getEPG") {
    IPS_LogMessage("getEPG","Popup");
    $EPG = '<table width="100%">';
    $i = 0;
    foreach($data->Buffer->events as $event) {
      if($i % 2 == 0) {
        $EPG .= '<tr style="background-color:#000000; color:#ffffff;"><td>';
      }
      else {
        $EPG .= '<tr style="background-color:#080808; color:#ffffff;"><td>';
      }
      $ChannelName = $event->channel_name;
      $EPG .= date("d.m.Y H:i",$event->start_time)."-".date("H:i",$event->start_time+$event->duration)." ".$event->title." - ".$event->short_text."</td></tr>";
      $i++;
    }
    $EPG .="</table>";
    WFC_SendPopup(50776, $ChannelName, $EPG);
  }
  if ($data->Action == "getChannels") {
    $test = (array)$data->Buffer;
    $Groups = array_keys($test);
    IPS_LogMessage("groups", $Groups[0]);
    $Channels = '<script type="text/javascript">
    function toggle(control){
      var elem = document.getElementById(control);

      if(elem.style.display == "none"){
        elem.style.display = "block";
      }else{
        elem.style.display = "none";
      }
    }
    </script>';
    $GroupNumber = 0;
    $Channels .= '<table width="100%">';
    foreach($Groups as $Group) {
      if($GroupNumber % 2 == 0) {
        $Channels .= '<tr style="background-color:#000000; color:#ffffff;"><td>';
      }
      else {
        $Channels .= '<tr style="background-color:#080808; color:#ffffff;"><td>';
      }
      $Channels .= '<a href="javascript:toggle(\''.$Group.'\')"> '.$Group.'</a>';
      $Channels .= '<br /><div id="'.$Group.'" style="display: none">';
      $i = 0;
      $Channels .= '<table width="100%"><tr width="20%">';
      foreach ($test[$Group] as $channel) {
        if ($i <= 4) {
          $Channels .=  "";
        }
        else {
          $i = 0;
          $Channels .= "</tr><tr>";
        }
        $Channels .= '';
        $Channels .= '<td><b>'.$channel->ChannelName.'</b>';
        $Channels .= '<br /><div class="button" style="border:1px solid #8bcf54; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
        background-color: #a9db80; background-image: -webkit-gradient(linear, left top, left bottom, from(#a9db80), to(#96c56f));
        background-image: -webkit-linear-gradient(top, #a9db80, #96c56f);
        background-image: -moz-linear-gradient(top, #a9db80, #96c56f);
        background-image: -ms-linear-gradient(top, #a9db80, #96c56f);
        background-image: -o-linear-gradient(top, #a9db80, #96c56f);
        background-image: linear-gradient(to bottom, #a9db80, #96c56f);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#a9db80, endColorstr=#96c56f);"'.$this->getWebhookLink($channel->ChannelID, "Play").'>Abspielen auf VDR';
        $Channels .='</div>';
        /** $Channels .='<div class="button" '.$this->getWebhookLink($channel->ChannelID).' style="border:1px solid #cb9b14; -webkit-border-radius: 3px; -moz-border-radius: 3px;border-radius: 3px;font-size:12px;font-family:arial, helvetica, sans-serif; padding: 10px 10px 10px 10px; text-decoration:none; display:inline-block;text-shadow: -1px -1px 0 rgba(0,0,0,0.3);font-weight:bold; color: #FFFFFF;
 background-color: #eab92d; background-image: -webkit-gradient(linear, left top, left bottom, from(#eab92d), to(#c79810));
 background-image: -webkit-linear-gradient(top, #eab92d, #c79810);
 background-image: -moz-linear-gradient(top, #eab92d, #c79810);
 background-image: -ms-linear-gradient(top, #eab92d, #c79810);
 background-image: -o-linear-gradient(top, #eab92d, #c79810);
 background-image: linear-gradient(to bottom, #eab92d, #c79810);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#eab92d, endColorstr=#c79810);"'.$this->getWebhookLink($channel->ChannelID, "Play").'>Programm';
        $Channels .='</div>'**/
        $Channels .='</td>';
        $i++;
      }
      $Channels .= "</table>";
      $GroupNumber++;
      $Channels .= '</td></tr>';
      $Channels .='</div>';
    }
    $Channels .='</table>';
    SetValue($this->GetIDForIdent("Kanaele") ,$Channels);
  }
}

public function EPGHook($ChannelID) {
  $this->SendDataToParent(json_encode(Array("DataID" => "{66900AB7-4164-4AB3-9F86-703A38CD5DA0}", "Action" => "getEPG", "Buffer" => $ChannelID)));
}

private function RegisterHook($WebHook, $TargetID)
{
  $ids = IPS_GetInstanceListByModuleID("{015A6EB8-D6E5-4B93-B496-0D3F77AE9FE1}");
  if (sizeof($ids) > 0)
  {
    $hooks = json_decode(IPS_GetProperty($ids[0], "Hooks"), true);
    $found = false;
    foreach ($hooks as $index => $hook)
    {
      if ($hook['Hook'] == $WebHook)
      {
        if ($hook['TargetID'] == $TargetID)
        return;
        $hooks[$index]['TargetID'] = $TargetID;
        $found = true;
      }
    }
    if (!$found)
    {
      $hooks[] = Array("Hook" => $WebHook, "TargetID" => $TargetID);
    }
    IPS_SetProperty($ids[0], "Hooks", json_encode($hooks));
    IPS_ApplyChanges($ids[0]);
  }
}

private function UnregisterHook($WebHook)
{
  $ids = IPS_GetInstanceListByModuleID("{015A6EB8-D6E5-4B93-B496-0D3F77AE9FE1}");
  if (sizeof($ids) > 0)
  {
    $hooks = json_decode(IPS_GetProperty($ids[0], "Hooks"), true);
    $found = false;
    foreach ($hooks as $index => $hook)
    {
      if ($hook['Hook'] == $WebHook)
      {
        $found = $index;
        break;
      }
    }
    if ($found !== false)
    {
      array_splice($hooks, $index, 1);
      IPS_SetProperty($ids[0], "Hooks", json_encode($hooks));
      IPS_ApplyChanges($ids[0]);
    }
  }
}
}
?>
