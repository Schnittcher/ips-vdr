<?

require_once(__DIR__ . "/../cVDRRequest.php");

class IPS_VDRIO extends IPSModule {

	public function Create(){
		//Never delete this line!
		parent::Create();
		//SVDR Zugangsdaten
		$this->RegisterPropertyString("host", "");
		$this->RegisterPropertyString("port", "2001");
		$this->RegisterTimer("UpdateRecordings", 160000, 'VDRIO_getRecords($_IPS[\'TARGET\']);');
		$this->RegisterTimer("UpdateSystemInfo", 160000, 'VDRIO_getSystemInfo($_IPS[\'TARGET\']);');
		$this->RegisterTimer("UpdateChannels", 86400, 'VDRIO_getChannels($_IPS[\'TARGET\']);');
		//$this->RegisterTimer("UpdateEPG", 60000, 'VDRIO_getEvents($_IPS[\'TARGET\']);');
	}
	public function ApplyChanges() {
		//Never delete this line!
		parent::ApplyChanges();
	}

	public function ForwardData($JSONString) {
		$data = json_decode($JSONString);
		if ($data->Action == "DeleteRecords") {
			$this->deleteRecords($data->Buffer);
		}
		if ($data->Action == "playRecord") {
			$this->playRecording($data->Buffer);
		}
		if ($data->Action == "remoteButtonClick") {
			$this->remoteButtonClick($data->Buffer);
		}
		if ($data->Action == "Station") {
			$this->Station($data->Buffer);
		}
		if ($data->Action == "getEPG") {
			$this->getEPG($data->Buffer);
		}
		if ($data->Action == "ChannelInfo") {
			$this->getChannelInfo();
		}
	}

	public function getSystemInfo() {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$SystemInfo = $Request->getRequest("info.json");
		if ($SystemInfo <> false) {
			$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action"=> "getSystemInfo", "Buffer" => $SystemInfo)));
		}
		else
		{
			IPS_LogMessage("getSystemInfo", "Server offline");
		}
	}

	public function getRecords() {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$Records = $Request->getRequest("recordings.json");
		if ($Records <> false) {
			$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action"=> "getRecordings", "Buffer" => $Records)));
		}
		else {
			IPS_LogMessage("getRecords", "Server offline");
		}
	}

	public function getChannelInfo() {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$SystemInfo = $Request->getRequest("info.json");
		$ChannelID = $SystemInfo->channel;
		IPS_LogMessage("ChannelID", $ChannelID);
		IPS_LogMessage("URL","events/".$ChannelID.".json?chevents=1");
		$EPG = $Request->getRequest("events/".$ChannelID.".json?chevents=1");
		IPS_LogMessage("getChannelInfo", $EPG->events[0]->channel_name);
		$ChannelInfo["Channel"] = $EPG->events[0]->channel_name;
		$ChannelInfo["Titel"] = $EPG->events[0]->title;
		$ChannelInfo["Plot"] = $EPG->events[0]->short_text;
		$ChannelInfo["Dauer"] = $EPG->events[0]->duration;

		$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action"=> "getChannelInfo", "Buffer" => $ChannelInfo)));
	}

	public function getChannels() {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$ChannelsJson = $Request->getRequest("channels.json");
		if ($ChannelsJson <> false) {
			$Channels = [];
			foreach ($ChannelsJson->channels as $channel) {
				$Channels[$channel->group][] = ['ChannelName' => $channel->name, 'ChannelNumber' => $channel->number, 'ChannelStream' => $channel->stream, 'ChannelID' => $channel->channel_id];
			}
			$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action"=> "getChannels", "Buffer" => $Channels)));
		}
		else {
			IPS_LogMessage("getChannel", "Server offline");
		}
	}

	public function getEPG($ChannelID) {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$EPG = $Request->getRequest("events/".$ChannelID.".json?chevents=10");
		if ($EPG <> false) {
			//	return $EPG;
			$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action" => "getEPG", "Buffer" => $EPG)));
		}
		else {
			IPS_LogMessage("getEPG", "Server offline");
		}
	}

	public function deleteRecords($RecordingID) {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$Request->delete("recordings/".$RecordingID."?syncId=Symcon");
		//$this->SendDataToChildren(json_encode(Array("DataID" => "{A09538DA-3DAB-4E0B-93FF-30C0E3B374D6}", "Action"=> "deleteRecordings", "Buffer" => "test")));
	}
	public function playRecording($RecordingID) {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$Request->getRequest("recordings/play/".$RecordingID);
	}

	public function remoteButtonClick($Button) {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		$Request->set("remote/".$Button);
	}

	public function Station($Station) {
		$Request = new cVDRRequest($this->ReadPropertyString("host"), $this->ReadPropertyString("port"));
		IPS_LogMessage("Stationen", $Station);
		$ChannelsJson = $Request->getRequest("channels.json");
		if ($ChannelsJson <> false) {
			$Channels = [];
			foreach ($ChannelsJson->channels as $channel) {
				$Channels[$channel->group][] = ['ChannelName' => $channel->name, 'ChannelNumber' => $channel->number, 'ChannelStream' => $channel->stream, 'ChannelID' => $channel->channel_id];
				if ($channel->number == $Station) {
					$Request->set("remote/switch/".$channel->channel_id);
					exit;
				}
			}
		}
	}
}
?>
